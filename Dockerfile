FROM node:20

COPY . /project

WORKDIR /project
RUN npm ci
RUN npm run build

ENV ROTESTEAMBOT_CONFIG_DIR=/data
ENV ROTESTEAMBOT_DATA_DIR=/data

CMD ["node", "build/main.js"]