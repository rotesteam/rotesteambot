[![Latest Release](https://gitlab.com/rotesteam/rotesteambot/-/badges/release.svg)](https://gitlab.com/rotesteam/rotesteambot/-/releases)

# RotesTeamBot - MatrixBot um Keycloak-Gruppenmitglieder in ihre zugehörigen Matrix-Spaces einzuladen

## Kurze Zusammenfassung

Angestoßen durch den Benutzer selbst oder über die bereitgestellte REST-Schnittstelle, überprüft der Bot, ob der Benutzer Mitglied aller Spaces ist, die seinen Keycloak-Gruppen zugeordnet sind.
Dazu werden die Gruppen abgefragt und von jeder Gruppe das `MATRIX_SPACES` Attribut ausgelesen.
In `MATRIX_SPACES` steht ein JSON-Array, das die IDs der Spaces als Strings enthält.
Das Attribut wird nicht vom Bot geschrieben, sondern durch andere Tools oder vorerst von Hand.

Über eine REST Schnittstelle können die Space-IDs, in denen ein gegebener Benutzer Admin ist abgefragt werden. Außerdem kann eine Gruppe, um den Bot auf Änderungen aufmerksam zu machen.

# Entwicklung

- Es lässt sich gut mit VSCodium/VSCode entwickeln. (Nötig ist das aber nicht)
- Npm muss installiert sein. Anschließend `npm install` ausführen
- Am besten man testet mit einem lokalen Matrix-Server in einem podman/docker container. Mit dem skript auth.ts kann man sich einen Access-Token holen. Den aus Element sollte man nicht nehmen, weil dann die Kryptografie in den Bot-Chats aktiviert wird. Das verkompliziert den Bot unnötigt.
- Mit dem Befehl `npm run start:dev` wird der Bot immer neugestartet, wenn man eine Änderung macht. (Dieses Befehl kann man auch von VSCodium/VSCode ausführen lassen.)

# REST Schnittstelle

Es können ein paar Funktionen über REST aufgerufen werden. Dazu muss der "SharedSecret" Header mit dem konfigurierten Passwort befüllt werden.

## TODO

- Auch Benutzer in Subgruppen informieren, wenn eine Gruppe verändert wird.

# Deployment

Das letzte Image der [Releases](https://gitlab.com/rotesteam/rotesteambot/-/releases) nutzen.

Unter /data ein Verzeichnis/Volume mounten und dort eine bot-config.yml ablegen (siehe [Beispiel Datei](./bot-config-example.yml)).

Ausschnitt aus docker-compose.yml:

```
services:
  rotesteambot:
    image: "registry.gitlab.com/rotesteam/rotesteambot/rotesteambot:INSERT_VERSION"
    volumes:
      - ./rotesteambot:/data
      # The Bot will then use ./rotesteambot/bot-config.yml to configure itself
    expose:
      - 11742 # the port in bot-config.yml
```

In Keycloak einen client anlegen, dort die service-accounts-option aktivieren und die `view-users, query-users, query-groups` Rollen des realm-management-Clients vergeben.
