if ! test -d data; then 
    mkdir data
    podman run -it --rm \
        -v "$(pwd)/data":/data \
        -e SYNAPSE_SERVER_NAME=localhost \
        -e SYNAPSE_REPORT_STATS=no \
        matrixdotorg/synapse:latest generate
fi
podman run -d --rm --name synapse \
    -v "$(pwd)/data":/data:z \
    -p 8008:8008 \
    matrixdotorg/synapse:latest
