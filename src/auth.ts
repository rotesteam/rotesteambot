import {MatrixAuth} from 'matrix-bot-sdk';
import {exit} from 'process';
/**
 * This script can be used to retrieve an access token from a Matrix-Server.
 *
 * To use it first run `npm run build` in the project root and then `node build/auth.js <homeserverUrl> <username> <password>`
 *
 */

if (process.argv.length != 5) {
  console.error(
    'Error: 3 arguments are needed: <homeserverUrl> <username> <password>'
  );
  console.error(process.argv);
  exit(-1);
}
const homeserverUrl = process.argv[2];

const username = process.argv[3];
const password = process.argv[4];

const auth = new MatrixAuth(homeserverUrl);

const a = await auth.passwordLogin(username, password);
console.log(
  'Successfully logged in. This is the access token, please store it in the bot-config.yml file:'
);
console.log(a.accessToken);
