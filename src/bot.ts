import * as crypto from 'crypto';
import {fileTypeFromBuffer} from 'file-type';
import {readFile} from 'fs/promises';
import {
  AutojoinRoomsMixin,
  AutojoinUpgradedRoomsMixin,
  IStorageProvider,
  LogService,
  MatrixClient,
  MatrixError,
  PowerLevelAction,
  Space,
} from 'matrix-bot-sdk';
import {MatrixConfig, getConfigDir} from './config.js';
import Keycloak from './keycloak.js';
import {ExternalSpaceView} from './restendpoint.js';
import Spaces, {ExternalSpaceId} from './spaces.js';

type allowedSpacesResolver = (kcId: string) => Promise<ExternalSpaceId[]>;

function escapeText(str: string) {
  return str.replace(/[\\$'"]/g, '\\$&');
}

interface Command {
  adminCommand: boolean;
  name: string;
  /**
   * Andere namen unter dem dieser Befehl auch ausgeführt werden soll.
   * */
  alias?: string[];
  /**
   * Bei einem unbekannten Befehl, oder einer unbekannten Nachricht, wird der Befehl mit isHelpCommand == true ausgeführt.
   * */
  isHelpCommand?: boolean;
  /**
   * Ob der Befehl in der Befehlübersicht angezeigt werden soll.
   * */
  hidden?: boolean;
  /**
   * Beschreibung für die Befehlübersicht.
   * */
  description: string;
  execute(sender: string, roomid: string, event: any): Promise<unknown>;
}

export interface GroupMemberOverview {
  [index: string]: SpaceMemberOverview;
}
export interface SpaceMemberOverview {
  joinedButNoGroupMember: string[];
  joinedAndGroupMember: string[];
  notJoinedButGroupMember: string[];
}

export default class RotesTeamBot {
  private client: MatrixClient;
  private checkNewUsersTimer: ReturnType<typeof setInterval> | null = null;
  private commands: Command[];

  constructor(
    private matrixConf: MatrixConfig,
    private storage: IStorageProvider,
    private spaces: Spaces,
    private keycloak: Keycloak
  ) {
    this.client = new MatrixClient(
      matrixConf.homeserverUrl,
      matrixConf.accessToken,
      storage
    );
    AutojoinRoomsMixin.setupOnClient(this.client);
    AutojoinUpgradedRoomsMixin.setupOnClient(this.client);
    this.client.on('room.message', async (roomId: string, event) => {
      if (!event['content']) return;

      if (this.client.dms.isDm(roomId)) {
        this.handleDirectMessage(roomId, event);
      }
    });

    this.client.on('room.upgraded', async (roomId: string, event) => {
      if (
        typeof event.content.predecessor.room_id === 'string' &&
        (await this.spaces.alreadyKnowsRoom(event.content.predecessor.room_id))
      ) {
        const oldRoomId: string = event.content.predecessor.room_id;
        spaces.updateID(oldRoomId, roomId);
      }
    });
    if (matrixConf.avatarPath) {
      this.setAvatar(getConfigDir() + matrixConf.avatarPath);
    }
    this.commands = this.createCommands();
  }

  public async start() {
    LogService.info('RotesTeamBot', 'Starting matrix-client...');
    await this.client.start(null);
    this.checkNewUsersTimer = setInterval(() => this.checkNewUsers(), 2000);
    LogService.info('RotesTeamBot', 'Matrix-client started');
  }

  public stop() {
    if (this.checkNewUsersTimer) {
      clearInterval(this.checkNewUsersTimer);
    }
    this.client.stop();
  }

  public async getSpacesOfGroup(
    keycloakGroupId: string
  ): Promise<ExternalSpaceView[]> {
    const externalSpaceIds =
      await this.keycloak.getSpacesOfGroup(keycloakGroupId);
    const extView = [];
    for (const space of externalSpaceIds) {
      extView.push(
        await this.asExternalSpace((await this.spaces.getRoomID(space))!)
      );
    }
    return extView;
  }

  public async inviteUserToSpace(kcUserID: string, spaceExt: ExternalSpaceId) {
    const mxID = await this.getMatrixID(kcUserID);
    if (!mxID) {
      throw new Error(`User ${kcUserID} could not be found`);
    }
    this.sendFreshInvite(mxID, await this.spaces.getRoomID(spaceExt));
  }

  public async getManagableSpacesOfKCUser(
    kcUserID: string
  ): Promise<ExternalSpaceView[]> {
    const mxID = await this.getMatrixID(kcUserID);
    if (!mxID) {
      throw new Error(`User ${kcUserID} could not be found`);
    }
    const botManagableSpaces = await this.getManagableSpacesOfBot();

    const userManagableSpaces: ExternalSpaceView[] = [];
    for (const space of botManagableSpaces) {
      if (await this.isUserAdmin(mxID, space.roomId)) {
        userManagableSpaces.push(await this.asExternalSpace(space.roomId));
      }
    }
    return userManagableSpaces;
  }

  public async prepareSpace(spaceExt: ExternalSpaceId) {
    const space = await this.asSpaceOrNull(
      await this.spaces.getRoomID(spaceExt)
    );
    if (!space) {
      throw new Error(`Could not resolve ${spaceExt.id} as a Space`);
    }
    const botID = await this.client.getUserId();
    const hasPermission = (action: PowerLevelAction) =>
      this.client.userHasPowerLevelForAction(botID, space.roomId, action);
    if (
      !(await hasPermission(PowerLevelAction.Kick)) ||
      !(await hasPermission(PowerLevelAction.Invite))
    ) {
      LogService.info(
        'RotesTeamBot',
        `Making the bot admin of space ${space.roomId} using synapse admin-api`
      );
      await this.client.adminApis.synapse.makeRoomAdmin(space.roomId);
    }
  }

  public async inviteAll(kcGroupID: string) {
    const extSpaces = await this.keycloak.getSpacesOfGroup(kcGroupID);
    if (extSpaces.length == 0) {
      return;
    }
    const spaces = await this.getInternalSpaceIds(extSpaces);
    for (const kcUserID of await this.keycloak.getGroupMemberIDs(kcGroupID)) {
      const mxID = await this.getMatrixID(kcUserID);
      if (mxID) {
        for (const space of await this.getMissingSpaces(mxID, spaces)) {
          this.sendFreshInvite(mxID, space);
        }
      }
    }
  }

  public async checkUser(
    kcID: string,
    allowedSpacesResolver: allowedSpacesResolver = u =>
      this.keycloak.getAllowedSpaces(u)
  ) {
    const mxID = await this.getMatrixID(kcID);
    if (mxID) {
      const missingSpaces = await this.loadMissingSpaces(
        kcID,
        mxID,
        allowedSpacesResolver
      );
      for (const space of missingSpaces) {
        await this.sendFreshInvite(mxID, space.id);
      }
    }
  }

  public async getMemberOverview(
    keycloakGroup: string
  ): Promise<GroupMemberOverview> {
    const kcCache = this.keycloak.newKeycloakCache();
    const kcMembers = new Set(
      await this.keycloak.getGroupMemberIDs(keycloakGroup)
    );
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const botThis = this;
    const overview: GroupMemberOverview = {};
    const memberToKCIDResolver = new (class {
      cache: Map<string, string | null> = new Map();
      async get(mxID: string): Promise<string | null> {
        if (this.cache.has(mxID)) {
          return this.cache.get(mxID)!;
        } else {
          this.cache.set(mxID, await botThis.getKCId(mxID));
          return this.get(mxID);
        }
      }
    })();
    for (const space of await this.getInternalSpaceIds(
      await kcCache.getSpacesOfGroup(keycloakGroup)
    )) {
      const spaceMembersInt = await this.client.getJoinedRoomMembers(space);
      const spaceMembers = new Set(
        await Promise.all(
          spaceMembersInt.map(id => memberToKCIDResolver.get(id))
        )
      );
      const overViewOfSpace: SpaceMemberOverview = {
        joinedButNoGroupMember: Array.from(spaceMembers)
          .filter(m => m && !kcMembers.has(m))
          .map(m => m!),
        joinedAndGroupMember: Array.from(spaceMembers)
          .filter(m => m && kcMembers.has(m))
          .map(m => m!),
        notJoinedButGroupMember: Array.from(kcMembers).filter(
          m => m && !spaceMembers.has(m)
        ),
      };
      overview[(await this.spaces.getExternalID(space)).id] = overViewOfSpace;
    }
    return overview;
  }

  public async createNewSpace(
    kcID: string,
    spaceName: string
  ): Promise<ExternalSpaceView> {
    const mxID = await this.getMatrixID(kcID);
    if (!mxID) {
      throw new Error(`Couldnt get mxID of User ${kcID}`);
    }
    const space = await this.client.createSpace({
      isPublic: false,
      name: spaceName,
      invites: [mxID],
    });
    await this.client.setUserPowerLevel(mxID, space.roomId, 100);
    return await this.asExternalSpace(space.roomId);
  }

  public async notifyInviteUsed(
    inviteLabel: string,
    groupName: string,
    usedByName: string,
    informUsersKCID: string[]
  ): Promise<void> {
    for (const kcId of informUsersKCID) {
      const mxId = await this.getMatrixID(kcId);
      if (mxId) {
        const dmID = await this.client.dms.getOrCreateDm(mxId);
        this.client.sendNotice(
          dmID,
          `${usedByName} hat den einladungslink ${inviteLabel} für die Gruppe ${groupName} eingelöst und ist der Gruppe beigetreten.`
        );
      }
    }
  }

  private async checkNewUsers() {
    const userLoader = this.client.adminApis.synapse.listAllUsers({
      order_by: 'creation_ts',
      limit: 10,
      dir: 'b',
    } as any);
    const lastCheckedUserTimestamp = Number(
      await this.storage.readValue('matrix-bot-newest-checked-user-ts')
    );
    let newHighestCheckedTimestamp: number | undefined = undefined;
    for await (const user of userLoader) {
      const userCreationTimestamp = (user as any).creation_ts;
      newHighestCheckedTimestamp = Math.max(
        newHighestCheckedTimestamp ?? 0,
        userCreationTimestamp
      );
      if (!lastCheckedUserTimestamp) {
        // On first run don't check, otherwise we would run through all users
        break;
      }
      if (userCreationTimestamp <= lastCheckedUserTimestamp) {
        LogService.debug(
          'RotesTeamBot',
          'Stopping listing of new users at user ' +
            user.name +
            ' because they are already known'
        );
        break;
      }
      const kcID = await this.getKCId(user.name);
      if (kcID) {
        LogService.debug(
          'RotesTeamBot',
          'Checking new discovered user ' + user.name
        );
        await this.checkUser(kcID);
      }
    }
    if (newHighestCheckedTimestamp) {
      await this.storage.storeValue(
        'matrix-bot-newest-checked-user-ts',
        newHighestCheckedTimestamp + ''
      );
    }
  }

  private createCommands(): Command[] {
    return [
      {
        adminCommand: false,
        name: 'einladen',
        alias: ['!einladen'],
        description:
          'Dich in alle deine ausstehenden Spaces(Communities) einladen.',
        execute: async (sender, roomId, event) => {
          try {
            const kcID = await this.getKCId(sender);
            if (kcID) {
              const allowedSpaces = await this.getInternalSpaceIds(
                await this.keycloak.getAllowedSpaces(kcID)
              );
              let inviteCount = 0;
              for (const space of await this.getMissingSpaces(
                sender,
                allowedSpaces
              )) {
                await this.client.inviteUser(sender, space);
                await this.client.sendNotice(
                  roomId,
                  'Ich habe dich in den Space ' +
                    (await this.getRoomName(space)) +
                    ' eingeladen.'
                );
                inviteCount++;
              }
              if (inviteCount == 0) {
                await this.client.sendNotice(
                  roomId,
                  'Du hast keine ausstehenden Spaces.'
                );
              }
            } else {
              await this.client.sendNotice(
                roomId,
                'Ich konnte das Konto zu deinem Matrix-Konto nicht bestimmen. Eventuell schreibst du mir von einem anderen Matrix-Server oder etwas ist nicht korrekt konfiguriert.'
              );
              LogService.warn(
                'RotesTeamBot',
                `Konnte die KC-ID von Benutzer ${sender} nicht herausfinden.`
              );
            }
          } catch (e) {
            LogService.error(
              'RotesTeamBot',
              'Fehler beim Abfragen und Mappen der erlaubten Matrix-Spaces',
              sender,
              e
            );
            this.client.sendNotice(
              roomId,
              'Leider ist etwas schief gegangen. Probiere es später nochmal. Wenn das Problem nicht verschwindet melde dich bei den Administrator:innen.'
            );
          }
        },
      },
      {
        adminCommand: false,
        name: '!hilfe',
        isHelpCommand: true,
        description: 'Hilfe zum Bot anzeigen',
        alias: ['hilfe', '?', '/?', '!help', '!?', '/help', '/hilfe'],
        execute: async (sender, roomid, _event) => {
          await this.client.sendNotice(
            roomid,
            'Hallo. Ich bin ein klassischer Chat-Bot, der sich darum kümmert, dass jede Person in Matrix(dieser Messenger) Zugang zu den entsprechenden Spaces (Communities) erhält. Ich schreibe dich an wenn du in neue Spaces darfst.'
          );
          await this.sendCommandOverview(sender, roomid);
        },
      },
      {
        adminCommand: true,
        name: '!möglicheSpaces',
        description: 'Liste der Spaces erhalten, die ich verwalten kann.',
        execute: (_sender, roomId, _event) =>
          this.sendBotManagableSpacesList(roomId),
      },
      {
        adminCommand: true,
        name: '!alleBenutzerChecken',
        description:
          'Für alle Benutzer überprüfen ob sie in allen Spaces sind, in die sie dürfen und ggf. einladen.',
        execute: async (_sender, roomId, _event) => {
          await this.client.sendNotice(
            roomId,
            'Ich beginne alle Benutzer zu überprüfen...'
          );
          await this.checkSpacesOfAllUsers();
          await this.client.sendNotice(
            roomId,
            'Ich habe alle Benutzer überprüft und entsprechend Einladungen angeboten.'
          );
        },
      },
      {
        adminCommand: true,
        name: '!matrixId',
        description:
          'Die MatrixId eines Benutzers anhand der Keycloak-ID/username herausfinden.',
        execute: async (_sender, roomId, event) => {
          const body = event['content']['body'];
          if (typeof body === 'string') {
            const commandParsed = body.match(/!?matrixId (.+)/);
            if (commandParsed) {
              const kcID = commandParsed[1];
              if (kcID) {
                const mxID = await this.getMatrixID(kcID);
                if (mxID) {
                  await this.client.sendNotice(roomId, mxID);
                } else {
                  await this.client.sendNotice(
                    roomId,
                    'Konnte keine ID finden'
                  );
                }
              } else {
                await this.client.sendNotice(
                  roomId,
                  'Es muss eine Keycloak-UUID angegeben werden.'
                );
              }
            } else {
              await this.client.sendNotice(
                roomId,
                'Es muss eine Keycloak-UUID angegeben werden'
              );
            }
          }
        },
      },
      {
        adminCommand: false,
        name: '!stop',
        alias: ['stop'],
        hidden: true,
        description: '',
        execute: async (_sender, roomId, _event) => {
          await this.client.sendNotice(
            roomId,
            'Wenn ich etwas unerwünschtes tue, dann kontaktiere bitte die Admins.'
          );
        },
      },
      {
        adminCommand: false,
        name: '!test',
        alias: ['test'],
        description:
          'Antwortet mit dem Text Test. Optional kann über die Angabe einer Zahl, eine Verzögerung in Sekunden bis zur Antwort angegeben werden. Z.B !test 10, für eine Verzögerung von 10 Sekunden.',
        execute: async (sender, roomId, event) => {
          const body = event['content']['body'];
          if (typeof body === 'string') {
            const easterEgg =
              this.matrixConf.easterEggTargetUser === sender &&
              Math.random() < 0.5;
            const commandParsed = body.match(/!?test (\d*)/);
            if (commandParsed) {
              const delayArg = Number.parseInt(commandParsed[1]);
              if (delayArg >= 0 && delayArg <= 1200) {
                await this.client.sendNotice(
                  roomId,
                  `Ich ${
                    easterEgg
                      ? 'antworte, ähh ich meine ich wiehere'
                      : 'antworte dir'
                  } in ${delayArg} Sekunden${easterEgg ? '' : ' mit "Test"'}.`
                );
                setTimeout(() => {
                  const timeStr = new Intl.DateTimeFormat('de-DE', {
                    timeStyle: 'medium',
                  }).format(new Date());
                  this.client.sendText(
                    roomId,
                    `${easterEgg ? '🐎' : 'Test'} (${
                      easterEgg ? 'Wieherzeit' : 'Sendezeit'
                    } ${timeStr} Uhr)`
                  );
                }, delayArg * 1000);
              } else {
                await this.client.sendNotice(
                  roomId,
                  'Die Verzögerung muss zwischen 1-1200 Sekunden liegen.'
                );
              }
            } else {
              await this.client.sendNotice(roomId, 'Test');
            }
          }
        },
      },
      {
        adminCommand: false,
        name: '!echo',
        alias: ['echo'],
        description: 'Antwortet mit dem selben Text, der gesendet wurde.',
        execute: async (sender, roomId, event) => {
          const body = event['content']['body'];
          if (typeof body === 'string') {
            await this.client.sendNotice(roomId, body);
          }
        },
      },
    ];
  }
  private async sendCommandOverview(userId: string, roomid: string) {
    const isAdmin = this.isUserBotAdmin(userId);
    if (isAdmin) {
      await this.client.sendNotice(
        roomid,
        'Du bist als Bot-Admin hinterlegt, daher siehst du diese Nachricht und darfst Adminbefehle sehen sowie nutzen.'
      );
    }
    let html = '<p>Folgende Befehle verstehe ich:</p><ul>';
    for (const command of this.commands) {
      if ((command.adminCommand && !isAdmin) || command.hidden) {
        continue;
      }
      html += '<li>';
      html += '<strong>' + escapeText(command.name) + '</strong><br/>';
      if (isAdmin) {
        html +=
          'Nur für Admins: ' + (command.adminCommand ? 'Ja' : 'Nein') + '<br/>';
      }
      html += 'Beschreibung: ' + escapeText(command.description) + '';
      html += '</li>';
    }
    html += '</ul>';
    await this.client.sendHtmlNotice(roomid, html);
  }

  private async asExternalSpace(spaceId: string): Promise<ExternalSpaceView> {
    const avatarUrl = await this.getRoomAvatar(spaceId);
    return {
      spaceName: await this.getRoomName(spaceId),
      id: (await this.spaces.getExternalID(spaceId)).id,
      avatarUrl,
    };
  }

  private async getMissingSpaces(
    usermxID: string,
    spacesToConsider: string[]
  ): Promise<string[]> {
    const spacesNotIn = [];
    for (const space of spacesToConsider) {
      if (!(await this.isUserMember(usermxID, space))) {
        spacesNotIn.push(space);
      }
    }
    LogService.debug(
      'RotesTeamBot',
      `Missing spaces: ${spacesNotIn} from total of ${spacesToConsider.length} allowed`
    );
    return spacesNotIn;
  }

  private async handleDirectMessage(roomId: string, event: any) {
    const sender = event['sender'];
    if (sender == (await this.client.getUserId())) {
      return; // Don't need to process our own message
    }
    const body = event['content']['body'];
    if (typeof body === 'string') {
      const requestedCommand = body.toLowerCase().trim();
      const [command] = this.commands.filter(c =>
        [c.name, c.alias ?? []]
          .flat()
          .find(name => requestedCommand.startsWith(name.toLowerCase()))
      );
      if (command) {
        if (!command.adminCommand || this.isUserBotAdmin(sender)) {
          await command.execute(sender, roomId, event);
        } else {
          await this.client.sendNotice(
            roomId,
            'Du darfst diesen Befehl nicht nutzen. Du bist nicht als Admin in der Bot-Konfiguration hinterlegt.'
          );
        }
      } else {
        await this.client.sendNotice(roomId, 'Diesen Befehl kenne ich nicht.');
        await this.commands
          .filter(c => c.isHelpCommand)
          .at(0)
          ?.execute(sender, roomId, event);
      }
    }
  }

  private async checkSpacesOfAllUsers() {
    const fullRep = await this.keycloak.fetchFullRepresentation();
    for (const user of fullRep.allUsers) {
      await this.checkUser(user, u => fullRep.getAllowedSpaces(u));
    }
  }

  private async sendFreshInvite(mxID: string, space: string) {
    if (await this.isUserMember(mxID, space)) {
      return;
    }
    if (await this.isUserAlreadyInvited(mxID, space)) {
      await this.client.kickUser(mxID, space, 'Refreshing invite');
    }
    await this.client.inviteUser(mxID, space);
  }

  private async loadMissingSpaces(
    kcID: string,
    _mxID: string,
    allowedSpacesResolver: allowedSpacesResolver
  ) {
    const allowedSpaces = await this.getInternalSpaceIds(
      await allowedSpacesResolver(kcID)
    );
    const missingSpaces = await this.getMissingSpaces(kcID, allowedSpaces);
    return await Promise.all(
      missingSpaces.map(async space => ({
        id: space,
        name: await this.getRoomName(space),
      }))
    );
  }

  private getInternalSpaceIds(
    externalSpaces: ExternalSpaceId[]
  ): Promise<string[]> {
    return Promise.all(
      externalSpaces.map(extId => this.spaces.getRoomID(extId))
    );
  }

  private async isUserMember(userID: string, groupID: string) {
    return !!(await this.client.getJoinedRoomMembers(groupID)).includes(userID);
  }

  private async isUserAlreadyInvited(userID: string, groupID: string) {
    return !!(
      await this.client.getRoomMembers(groupID, undefined, ['invite'])
    ).find(m => m.membership == 'invite' && m.membershipFor == userID);
  }

  private isUserBotAdmin(matrixID: string) {
    return this.matrixConf.adminUsers.includes(matrixID);
  }

  private async sendBotManagableSpacesList(dmID: string) {
    const possibleSpaces = await this.getManagableSpacesOfBot();
    const allNames: [Space, string][] = await Promise.all(
      possibleSpaces.map(async space => [
        space,
        await this.getRoomName(space.roomId),
      ])
    );
    let html =
      '<p>In folgende Spaces kann ich einladen. Die ID für Keycloak muss in Keycloak eingetragen werden. Ich ordne ihr immer die aktuelle Matrix-ID des Spaces zu:</p>';
    html += '<table>';
    html +=
      '<thead><tr><th>Space-Name</th><th>Derzeitige interne Matrix ID</th><th>Dauerhafte ID für Keycloak-Config</th></tr></thead>';
    html += '<tbody>';
    for (const [space, spaceName] of allNames) {
      html += '<tr>';
      html += '<td>' + escapeText(spaceName) + '</td>';
      html += '<td>' + escapeText(space.roomId) + '</td>';
      html +=
        '<td>' +
        escapeText((await this.spaces.getExternalID(space.roomId)).id) +
        '</td>';
      html += '</tr>';
    }
    html += '</tbody>';
    html += '</table>';
    await this.client.sendHtmlNotice(dmID, html);
  }

  private async getRoomName(roomID: string): Promise<string> {
    const nameEvent = await this.client.getRoomStateEvent(
      roomID,
      'm.room.name',
      ''
    );
    return nameEvent['name'];
  }

  private async getRoomAvatar(roomID: string): Promise<string | null> {
    let avatarEvent = null;
    try {
      avatarEvent = await this.client.getRoomStateEvent(
        roomID,
        'm.room.avatar',
        ''
      );
    } catch (e: any) {
      if (e instanceof MatrixError && e.errcode == 'M_NOT_FOUND') {
        return null;
      } else {
        throw e;
      }
    }
    const url = avatarEvent['url'];
    return url ? url : null;
  }

  private async getManagableSpacesOfBot(): Promise<Space[]> {
    const rooms = await this.client.getJoinedRooms();
    const roomToSpacesPromises = rooms.map(roomId =>
      this.asSpaceOrNull(roomId)
    );
    const spaces = (await Promise.all(roomToSpacesPromises))
      .filter(spaceOrNull => !!spaceOrNull)
      .map(space => space!);

    return spaces;
  }

  private async asSpaceOrNull(roomID: string): Promise<Space | null> {
    // Haven't found a way to better determinie whether a room is a space
    try {
      return await this.client.getSpace(roomID);
    } catch (err: any) {
      if (err?.message == 'Room is not a space') {
        return null;
      } else {
        throw err;
      }
    }
  }

  private async isSpace(roomID: string): Promise<boolean> {
    return !!(await this.asSpaceOrNull(roomID));
  }

  private async isUserAdmin(mxUserID: string, roomId: string) {
    const powerLevelsEvent = await this.client.getRoomStateEvent(
      roomId,
      'm.room.power_levels',
      ''
    );
    if (!powerLevelsEvent) {
      throw new Error('No power level event found');
    }
    if (Number.isFinite(powerLevelsEvent['users']?.[mxUserID])) {
      return powerLevelsEvent['users'][mxUserID] >= 100;
    } else {
      return false;
    }
  }

  private async getKCId(matrixAccount: string): Promise<string | null> {
    if (!(await this.isLocalUser(matrixAccount))) {
      return null;
    }
    const user = await this.client.adminApis.synapse.getUser(matrixAccount);
    return (user.external_ids ?? [])
      .filter(
        exId => exId.auth_provider == this.matrixConf.keycloakAuthProviderName
      )
      .map(exId => exId.external_id)[0];
  }

  private async isLocalUser(mxID: string) {
    const ownID = await this.client.getUserId();
    const server = ownID.substring(ownID.lastIndexOf(':') + 1);
    return mxID.endsWith(server);
  }

  private async getMatrixID(keycloakUserID: string): Promise<string | null> {
    const authProvider = this.matrixConf.keycloakAuthProviderName;
    try {
      const {user_id: userId} = await this.client.doRequest(
        'GET',
        `/_synapse/admin/v1/auth_providers/${encodeURIComponent(
          authProvider
        )}/users/${encodeURIComponent(keycloakUserID)}`
      );
      if (typeof userId === 'string') {
        return userId;
      } else {
        throw new Error('Unexpected Response from Server');
      }
    } catch (e) {
      if (e instanceof MatrixError && e.errcode == 'M_NOT_FOUND') {
        LogService.debug(
          'RotesTeamBot',
          "Error while getting MXID of KC-ID='" + keycloakUserID + "'",
          e
        );
        return null;
      } else {
        throw e;
      }
    }
  }

  private async setAvatar(filePath: string): Promise<void> {
    const newAvatarBuffer = await readFile(filePath);

    const hashFromBuffer = (buff: Buffer) => {
      const hash = crypto.createHash('sha256');
      hash.update(buff);
      return hash.digest('hex');
    };

    const profile = await this.client.getUserProfile(
      (await this.client.getWhoAmI()).user_id
    );
    let currentAvatarHash = '';
    if (typeof profile.avatar_url === 'string') {
      try {
        const currentAvatar = await this.client.downloadContent(
          profile.avatar_url
        );
        currentAvatarHash = hashFromBuffer(currentAvatar.data);
      } catch (err) {
        LogService.error(
          'RotesTeamBot',
          'Error while downloading own avatar from ',
          profile.avatar_url,
          err
        );
      }
    }

    // Do we need upload the avatar or is it the same as the current?
    if (
      !currentAvatarHash ||
      currentAvatarHash != hashFromBuffer(newAvatarBuffer)
    ) {
      const fileType = await fileTypeFromBuffer(newAvatarBuffer);
      const newAvatarUrl = await this.client.uploadContent(
        newAvatarBuffer,
        fileType?.mime
      );
      await this.client.setAvatarUrl(newAvatarUrl);
      LogService.info('RotesTeamBot', 'Uploaded new avatar');
    } else {
      LogService.debug('RotesTeamBot', 'Avatar is up to date');
    }
  }
}
