import * as yaml from 'js-yaml';
import * as fs from 'fs';

import * as yup from 'yup';

// Define the Yup schemas
const MatrixConfigSchema = yup.object().shape({
  homeserverUrl: yup.string().required(),
  accessToken: yup.string().required(),
  keycloakAuthProviderName: yup.string().required(),
  adminUsers: yup.array().of(yup.string().required()).required(),
  avatarPath: yup.string(),
  easterEggTargetUser: yup.string(),
});

const KeycloakConfigSchema = yup.object().shape({
  clientId: yup.string().required(),
  clientSecret: yup.string().required(),
  // URL with protocol and path up to realm, eg https://domain.net/auth/,
  // where the path to the realm would be https://domain.net/auth/realms/<realm>
  keycloakURL: yup.string().required(),
  realm: yup.string().required(),
});

const RestEndpointConfigSchema = yup.object().shape({
  port: yup.number().required(),
  password: yup.string().required(),
});

const BotConfigSchema = yup.object().shape({
  matrix: MatrixConfigSchema.required(),
  keycloak: KeycloakConfigSchema.required(),
  restEndpoint: RestEndpointConfigSchema.required(),
});

export type MatrixConfig = yup.InferType<typeof MatrixConfigSchema>;
export type KeycloakConfig = yup.InferType<typeof KeycloakConfigSchema>;
export type RestEndpointConfig = yup.InferType<typeof RestEndpointConfigSchema>;
export type BotConfig = yup.InferType<typeof BotConfigSchema>;


export function getConfigDir(): string {
  return (process.env.ROTESTEAMBOT_CONFIG_DIR ?? '.') + '/';
}
export function getDataDir(): string {
  return (process.env.ROTESTEAMBOT_DATA_DIR ?? './data') + '/';
}

export function getConfig(fileName: string): BotConfig {
  const data = yaml.load(fs.readFileSync(fileName, 'utf8')) as any;

  return BotConfigSchema.validateSync(data);
}
