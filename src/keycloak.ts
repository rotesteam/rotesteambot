import {IStorageProvider, LogService} from 'matrix-bot-sdk';
import fetch from 'node-fetch';

import querystring from 'querystring';
import {ExternalSpaceId} from './spaces.js';

/**
 * This module contains code to communicate with the keycloak server and access resources over the Keycloak Admin REST Api
 */

const uriEncode = encodeURIComponent;

interface KeycloakParams {
  clientId: string;
  clientSecret: string;
  // URL with protocol and path up to realm, eg https://domain.net/auth/,
  // where the path to the realm would be https://domain.net/auth/realms/<realm>
  keycloakURL: string;
  realm: string;
  storage: IStorageProvider;
}

interface KCGroup {
  name: string;
  id: string;
  path: string;
  attributes: any;
  subGroups: KCGroup[] | null;
}

export default class Keycloak {
  private params: KeycloakParams;
  private accessToken: AccessToken;

  constructor(parameters: KeycloakParams) {
    this.params = parameters;
    this.accessToken = new AccessToken(parameters);
  }

  public newKeycloakCache(): KeycloakCache {
    return new KeycloakCache(this);
  }

  public async getAllowedSpaces(
    keycloakUserId: string
  ): Promise<ExternalSpaceId[]> {
    const groupsWithAccess = await this.getGroupsWithAccess(keycloakUserId);
    return groupsWithAccess.flatMap(group => this.getConfiguredSpaces(group));
  }

  public async getGroupMemberIDs(kcGroupID: string): Promise<string[]> {
    const users: any[] = await this.fetchResourceExhaustively(
      `/groups/${uriEncode(kcGroupID)}/members`,
      {briefRepresentation: true}
    );
    return users.map(user => user.id);
  }

  public async getSpacesOfGroup(kcGroupID: string): Promise<ExternalSpaceId[]> {
    return this.getConfiguredSpaces(await this.getGroupById(kcGroupID));
  }

  getConfiguredSpaces(group: KCGroup): ExternalSpaceId[] {
    // The attribute can be defined several times
    // Therefore the attribute is an array of values, where each value is a possible encoded JSON-Array of Space IDs
    const spacesAttributes: string[] =
      group.attributes?.['MATRIX_SPACES'] ?? [];
    const rawValues = spacesAttributes.flatMap(
      jsonArray => JSON.parse(jsonArray) ?? []
    );
    const spaces = Array.from(new Set(rawValues)).map(
      id => new ExternalSpaceId(id)
    );
    LogService.debug('keycloak', `Group ${group.id} has spaces ${spaces}`);
    return spaces;
  }

  private getGroupsWithAccess(keycloakUserId: string): Promise<KCGroup[]> {
    return this.getDirectGroups(keycloakUserId);
  }

  async getGroupByPath(path: string): Promise<KCGroup> {
    return (await this.fetchResource(`/group-by-path/${path}`)) as KCGroup;
  }

  private async getDirectGroups(keycloakUserId: string): Promise<KCGroup[]> {
    const data = await this.fetchResourceExhaustively(
      `/users/${uriEncode(keycloakUserId)}/groups`,
      {briefRepresentation: false}
    );
    if (
      !Array.isArray(data) ||
      data.find(group => !group?.id || !group?.name)
    ) {
      throw new Error(
        `getDirectGroups() unknown response: ${JSON.stringify(data)}`
      );
    }
    return data;
  }

  async getGroupById(keycloakGroupId: string): Promise<KCGroup> {
    return (await this.fetchResource(
      `groups/${uriEncode(keycloakGroupId)}`
    )) as KCGroup;
  }

  public async fetchFullRepresentation() {
    const groupCache = this.newKeycloakCache();
    await groupCache.preCacheAllGroups();

    const directGroupsOfUser = new Map<string, Set<KCGroup>>();
    for (const group of groupCache.getKnownGroups()) {
      for (const user of await this.getGroupMemberIDs(group.id)) {
        let groupSet = directGroupsOfUser.get(user);
        if (!groupSet) {
          groupSet = new Set();
          directGroupsOfUser.set(user, groupSet);
        }
        groupSet.add(group);
      }
    }

    return {
      allUsers: directGroupsOfUser.keys(),
      getAllowedSpaces: async (user: string) => {
        return Array.from(directGroupsOfUser.get(user)!).flatMap(group =>
          this.getConfiguredSpaces(group)
        );
      },
    };
  }

  private static indexGroupByPath(
    group: KCGroup,
    pathToGroup: Map<string, KCGroup>
  ) {
    pathToGroup.set(group.path, group);
    (group.subGroups ?? []).forEach(subgroup =>
      Keycloak.indexGroupByPath(subgroup, pathToGroup)
    );
  }

  async fetchResourceExhaustively(
    resource: string,
    queryArgs: {[index: string]: any} = {}
  ): Promise<any[]> {
    // Initialise with sane defaults
    queryArgs = {
      first: 0, // Actually an offset
      max: 100,
      ...queryArgs,
    };
    const response = await this.fetchResource(resource, queryArgs);
    if (Array.isArray(response)) {
      if (response.length == queryArgs.max) {
        const nextOffset = parseInt(queryArgs['first']) + 1;
        response.push(
          await this.fetchResourceExhaustively(resource, {
            ...queryArgs,
            first: nextOffset,
          })
        );
      }
      return response;
    } else {
      throw new Error(
        `Expecting an array to be returned from ${resource} while fetching the resource exhaustively, but received: ${response}`
      );
    }
  }

  async fetchResource(
    resource: string,
    queryArgs: {[index: string]: any} = {}
  ): Promise<any> {
    const url =
      this.getBaseURL() + resource + '?' + querystring.stringify(queryArgs);
    const response = await fetch(url, {
      headers: {
        Authorization: `Bearer ${await this.accessToken.get()}`,
      },
    });
    if (response.status != 200) {
      throw new Error(
        `Error response ${response.status} - ${
          response.statusText
        } while fetching ${url}. Response-body: ${await response.text()}`
      );
    } else {
      return response.json();
    }
  }

  private getBaseURL() {
    return `${this.params.keycloakURL}/admin/realms/${uriEncode(
      this.params.realm
    )}/`;
  }
}

class KeycloakCache {
  private groupsByPath: Map<string, KCGroup> = new Map();
  private groupsById: Map<string, KCGroup> = new Map();

  constructor(private keycloak: Keycloak) {}

  getKnownGroups(): Iterable<KCGroup> {
    return this.groupsByPath.values();
  }

  public async getSpacesOfGroup(
    keycloakGroup: string
  ): Promise<ExternalSpaceId[]> {
    if (this.groupsById.has(keycloakGroup)) {
      return this.keycloak.getConfiguredSpaces(
        this.groupsById.get(keycloakGroup)!
      );
    } else {
      this.indexGroup(await this.keycloak.getGroupById(keycloakGroup));
      return this.getSpacesOfGroup(keycloakGroup);
    }
  }

  async preCacheAllGroups() {
    const allGroups: KCGroup[] = await this.keycloak.fetchResourceExhaustively(
      '/groups',
      {briefRepresentation: false}
    );
    allGroups.forEach(group => this.indexGroup(group));
  }

  async getFullPath(leafGroup: KCGroup): Promise<KCGroup[]> {
    function extractPrefixes(path: string): string[] {
      const prefixes = [path];
      if (path.lastIndexOf('/') > 0) {
        prefixes.push(
          ...extractPrefixes(path.substring(0, path.lastIndexOf('/')))
        );
      }
      return prefixes;
    }

    const prefixes = extractPrefixes(leafGroup.path);

    const sortedPrefixes = Array.from(prefixes).sort(
      (a, b) => a.length - b.length
    );

    // By fetching the shortest first we have a chance to get the subgroups without making extra calls for them
    const groupsInPath = [];
    for (const path of sortedPrefixes) {
      if (!this.groupsByPath.has(path)) {
        this.indexGroup(await this.keycloak.getGroupByPath(path));
      }
      groupsInPath.push(this.groupsByPath.get(path)!);
    }

    return groupsInPath;
  }

  private indexGroup(group: KCGroup) {
    this.groupsByPath.set(group.path, group);
    this.groupsById.set(group.id, group);
    (group.subGroups ?? []).forEach(subgroup => this.indexGroup(subgroup));
  }
}

function veryNearFuture() {
  return dateInSeconds(1);
}

function dateInSeconds(seconds: number) {
  const date = new Date();
  date.setSeconds(date.getSeconds() + seconds);
  return date;
}

/**
 * Fetches and caches the KC access token.
 */
class AccessToken {
  private params: KeycloakParams;

  private persistentLoadDone: Promise<void>;
  private token = '';
  private valid_until: Date = new Date(0);

  constructor(params: KeycloakParams) {
    this.params = params;
    this.persistentLoadDone = this.loadStoredAccessToken();
  }

  async get(): Promise<string> {
    // TODO it seems possible two access tokens are fetched when the last one is not valid anymore.
    // There is no indication whether we are already fetching one
    await this.persistentLoadDone; // Waiting for possible side effect
    if (this.accessTokenValid()) {
      LogService.debug('keycloak', 'using cached access token');
      return this.token;
    }
    const url = `${this.params.keycloakURL}/realms/${uriEncode(
      this.params.realm
    )}/protocol/openid-connect/token`;
    LogService.debug('keycloak', `fetching new access token on url=${url}`);
    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: `grant_type=client_credentials&client_id=${uriEncode(
        this.params.clientId
      )}&client_secret=${uriEncode(this.params.clientSecret)}`,
    });

    const data: any = await response.json();
    if (
      response.status != 200 ||
      typeof data !== 'object' ||
      !data?.access_token ||
      !data?.expires_in
    ) {
      throw new Error(
        `received an unexpected response while fetching ${url} (Status ${
          response.status
        }): ${JSON.stringify(data)}`
      );
    }

    this.token = data.access_token;
    this.valid_until = dateInSeconds(data.expires_in);

    await this.storeAccessToken();

    return this.token;
  }

  private accessTokenValid(): boolean {
    return this.valid_until > veryNearFuture();
  }

  private async loadStoredAccessToken(): Promise<void> {
    const storedTokens: [AccessTokenStorage] = await this.loadStoredData();

    const storedToken = storedTokens.find(
      storedToken =>
        storedToken.url == this.params.keycloakURL &&
        storedToken.realm == this.params.realm
    );
    if (storedToken) {
      this.token = storedToken.token;
      this.valid_until = new Date(storedToken.validUntil);
      LogService.debug('keycloak', 'restored persisted access token');
    }
  }

  private async loadStoredData(): Promise<[AccessTokenStorage]> {
    const storedValueString = await this.params.storage.readValue(
      'keycloak-accesstokens'
    );
    return JSON.parse(storedValueString ?? '[]');
  }

  private async storeAccessToken() {
    LogService.debug('keycloak', 'persisting access token');
    const storedTokens = await this.loadStoredData();
    const stillValidTokens = storedTokens.filter(
      storedToken => new Date(storedToken.validUntil) > new Date()
    );
    const previousToken = storedTokens.find(
      storedToken =>
        storedToken.url == this.params.keycloakURL &&
        storedToken.realm == this.params.realm
    );

    const token = {
      url: this.params.keycloakURL,
      // That way the date contains the timezone and can be reconverted to a Date correctly
      validUntil: this.valid_until.toISOString(),
      realm: this.params.realm,
      token: this.token,
    };
    if (previousToken) {
      stillValidTokens[stillValidTokens.indexOf(previousToken)] = token;
    } else {
      stillValidTokens.push(token);
    }
    this.params.storage.storeValue(
      'keycloak-accesstokens',
      JSON.stringify(stillValidTokens)
    );
  }
}

interface AccessTokenStorage {
  url: string;
  validUntil: string;
  realm: string;
  token: string;
}
