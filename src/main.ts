import {
  SimpleFsStorageProvider,
  LogService,
  LogLevel,
  RichConsoleLogger,
} from 'matrix-bot-sdk';

import Keycloak from './keycloak.js';
import {getConfig, getConfigDir, getDataDir} from './config.js';
import Spaces from './spaces.js';
import RotesTeamBot from './bot.js';
import RestEndpoint from './restendpoint.js';

const {
  matrix: matrixConf,
  keycloak: keycloakConfig,
  restEndpoint: restEndpointConfig,
} = getConfig(getConfigDir() + 'bot-config.yml');

// First things first: let's make the logs a bit prettier.
LogService.setLogger(new RichConsoleLogger());
// For now let's also make sure to log everything (for debugging)
LogService.setLevel(LogLevel.DEBUG);
// Also let's mute Metrics, so we don't get *too* much noise
LogService.muteModule('Metrics');

const storage = new SimpleFsStorageProvider(getDataDir() + 'bot.json');

const keycloak = new Keycloak({...keycloakConfig, storage: storage});
const spaces = new Spaces(getDataDir() + 'space-mapping.json');
const bot = new RotesTeamBot(matrixConf, storage, spaces, keycloak);

const restEndpoint = new RestEndpoint(restEndpointConfig, {
  getManagableSpaces(kcID) {
    return bot.getManagableSpacesOfKCUser(kcID);
  },
  inviteAll(kcGroupID) {
    return bot.inviteAll(kcGroupID);
  },
  spacesOfGroup(keycloakGroupId) {
    return bot.getSpacesOfGroup(keycloakGroupId);
  },
  checkUser(keycloakUser) {
    return bot.checkUser(keycloakUser);
  },
  getMemberOverview(keycloakGroup) {
    return bot.getMemberOverview(keycloakGroup);
  },
  inviteToSpace(keycloakUser, space) {
    return bot.inviteUserToSpace(keycloakUser, space);
  },
  createNewSpace(keycloakUser, spaceName) {
    return bot.createNewSpace(keycloakUser, spaceName);
  },
  prepareSpace(space) {
    return bot.prepareSpace(space);
  },
  notifyInviteUsed(inviteLabel, groupName, usedByName, informUsersKCID) {
    return bot.notifyInviteUsed(
      inviteLabel,
      groupName,
      usedByName,
      informUsersKCID
    );
  },
});

restEndpoint.start();
bot.start();

function gracefullShutdown() {
  LogService.info(
    'Main',
    'Gracefully shutting down, this might take up to 30 seconds...'
  );
  try {
    restEndpoint.stop();
  } catch (e) {
    LogService.error('Main', 'Error while shutting down Restendpoint', e);
  }
  try {
    bot.stop();
  } catch (e) {
    LogService.error('Main', 'Error while shutting down bot', e);
  }
}
process.once('SIGTERM', gracefullShutdown);
process.once('SIGINT', gracefullShutdown);
