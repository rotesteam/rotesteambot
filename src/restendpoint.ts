import express from 'express';
import {RestEndpointConfig} from './config.js';
import {LogService} from 'matrix-bot-sdk';
import {Application} from 'express';
import {Server} from 'http';
import {GroupMemberOverview} from './bot.js';
import {ExternalSpaceId} from './spaces.js';
import { array, string } from 'yup';
import bodyParser from 'body-parser';

export interface RestEndpointFunctions {
  getManagableSpaces(keycloakUserId: string): Promise<ExternalSpaceView[]>;
  inviteAll(keycloakGroup: string): Promise<void>;
  spacesOfGroup(keycloakGroupId: string): Promise<ExternalSpaceView[]>;
  checkUser(keycloakUser: string): Promise<void>;
  getMemberOverview(keycloakGroup: string): Promise<GroupMemberOverview>;
  inviteToSpace(keycloakUser: string, space: ExternalSpaceId): Promise<void>;
  createNewSpace(
    keycloakUser: string,
    spaceName: string
  ): Promise<ExternalSpaceView>;
  prepareSpace(space: ExternalSpaceId): Promise<void>;
  notifyInviteUsed(inviteLabel: string, groupName: string, usedByName: string, informUsersKCID: string[]): Promise<void>;
}

export interface ExternalSpaceView {
  spaceName: string;
  id: string;
  avatarUrl: string | null;
}

export default class RestEndpoint {
  private app: Application;
  private server: Server<any> | null = null;

  constructor(
    private conf: RestEndpointConfig,
    private callbacks: RestEndpointFunctions
  ) {
    if (!conf.password) {
      throw new Error('No Password given from config');
    }
    const app = express();

    app.all('*', (req, res, next) => {
      if (req.header('SharedSecret') == undefined) {
        res.status(403).send('No SharedSecret given in requestheader');
      } else if (req.header('SharedSecret') !== conf.password) {
        res.status(403).send('Wrong SharedSecret given.');
      } else {
        next();
      }
    });

    app.use(bodyParser.json())

    app.get('/users/:user/manageableSpaces', async (req, res, next) => {
      if (!req.params.user) {
        res.status(400).send('No User given');
      } else {
        LogService.debug(
          'RestEndpoint',
          'Loading managable Spaces of ',
          req.params.user
        );
        try {
          res.send(await this.callbacks.getManagableSpaces(req.params.user));
        } catch (e) {
          LogService.error(
            'RestEndpoint',
            'Error while loading manageable spaces of: ' + req.params.user,
            e
          );
          next('Internal Error'); // invoke error handler of the lib
        }
      }
    });

    app.post('/users/:user/check', async (req, res, next) => {
      if (!req.params.user) {
        res.status(400).send('No User given');
      } else {
        LogService.debug(
          'RestEndpoint',
          'Checking Spaces of User',
          req.params.user
        );
        try {
          await this.callbacks.checkUser(req.params.user);
          res.send('Checked user');
        } catch (e) {
          LogService.error(
            'RestEndpoint',
            'Error while checking user: ' + req.params.user,
            e
          );
          next('Internal Error'); // invoke error handler of the lib
        }
      }
    });

    app.post('/users/:user/invite', async (req, res, next) => {
      if (typeof req.query['space'] !== 'string') {
        res.status(400).send('No space in query params given');
        return;
      }
      LogService.debug(
        'RestEndpoint',
        `Inviting user=${req.params.user} to extSpace=${req.query['space']}`
      );
      try {
        await this.callbacks.inviteToSpace(
          req.params.user,
          new ExternalSpaceId(req.query['space'])
        );
        res.send('success');
      } catch (e) {
        LogService.error(
          'RestEndpoint',
          `Error while inviting user=${req.params.user} to extSpace=${req.query['space']}`,
          e
        );
        next('Internal Error'); // invoke error handler of the lib
      }
    });

    app.post('/users/:user/newSpace', async (req, res, next) => {
      if (typeof req.query['spaceName'] !== 'string') {
        res.status(400).send('No spaceName in query params given');
        return;
      }
      LogService.debug(
        'RestEndpoint',
        `Creating space ${req.query['spaceName']} for user=${req.params.user}`
      );
      try {
        // Has to be written to the space attribute by the caller
        res.send(
          await this.callbacks.createNewSpace(
            req.params.user,
            req.query['spaceName']
          )
        );
      } catch (e) {
        LogService.error(
          'RestEndpoint',
          `Error while creating space ${req.query['spaceName']} for user=${req.params.user}`,
          e
        );
        next('Internal Error'); // invoke error handler of the lib
      }
    });

    app.get('/groups/:group/memberOverview', async (req, res, next) => {
      if (!req.params.group) {
        res.status(400).send('No Group given');
        return;
      }
      LogService.debug(
        'RestEndpoint',
        `Creating memberOverview for Group=${req.params.group}`
      );
      try {
        res.send(await this.callbacks.getMemberOverview(req.params.group));
      } catch (e) {
        LogService.error(
          'RestEndpoint',
          `Error while getting memberOverview for group=${req.params.group}`,
          e
        );
        next('Internal Error'); // invoke error handler of the lib
      }
    });

    app.post('/groups/:group/inviteAll', async (req, res, next) => {
      if (!req.params.group) {
        res.status(400).send('No Group given');
      } else {
        LogService.debug(
          'RestEndpoint',
          'Checking users of Group ',
          req.params.group
        );
        try {
          await this.callbacks.inviteAll(req.params.group);
          res.send('Group members successfully checked');
        } catch (e) {
          LogService.error(
            'RestEndpoint',
            'Error while processing configChanged-push of group: ' +
              req.params.group,
            e
          );
          next('Internal Error'); // invoke error handler of the lib
        }
      }
    });

    app.get('/groups/:group/spaces', async (req, res, next) => {
      if (!req.params.group) {
        res.status(400).send('No Group given');
      } else {
        LogService.debug(
          'RestEndpoint',
          'Fetching Spaces of Group ',
          req.params.group
        );
        try {
          res.send(await this.callbacks.spacesOfGroup(req.params.group));
        } catch (e) {
          LogService.error(
            'RestEndpoint',
            'Error while fetching spaces of group: ' + req.params.group,
            e
          );
          next('Internal Error'); // invoke error handler of the lib
        }
      }
    });

    app.post('/notify/inviteUsed', async (req, res, next) => {
      const { usedByName, groupName, inviteLabel} = req.query
      if (typeof usedByName != 'string' || typeof groupName != 'string' || typeof inviteLabel != 'string') {
        res.status(400).send('usedByName, groupName and inviteLabel must be single values given over the queryString')
        return
      }

      if (array(string().required()).required().isValidSync(req.body)) {
        const informUsersKCID = req.body;
        LogService.debug(
          'RestEndpoint',
          `Notifiying kcUsers ${informUsersKCID} that ${usedByName} joined group ${groupName} using link with label ${inviteLabel}`
        );
        await callbacks.notifyInviteUsed(inviteLabel, groupName, usedByName, informUsersKCID);
        res.status(204).send()
        return
      } else {
        res.status(400).send('Body must JSON encoded array of keycloak user ids, got body ' + JSON.stringify(req.body))
        return;
      }
    })

    app.post('/spaces/:space/prepare', async (req, res, next) => {
      if (!req.params.space) {
        res.status(400).send('No Space given');
      } else {
        LogService.debug(
          'RestEndpoint',
          `Preparing space ${req.params.space} for management`
        );
        try {
          await this.callbacks.prepareSpace(
            new ExternalSpaceId(req.params.space)
          );
          res.send('success');
        } catch (e) {
          LogService.error(
            'RestEndpoint',
            `Error while preparing space ${req.params.space} for management`,
            e
          );
          next('Internal Error'); // invoke error handler of the lib
        }
      }
    });

    this.app = app;
  }

  start() {
    if (this.server) {
      throw new Error('Restendpoint already started');
    }
    LogService.info(
      'RestEndpoint',
      'Starting rest endpoint on port:',
      this.conf.port
    );
    this.server = this.app.listen(this.conf.port);
  }

  stop() {
    this.server?.close();
  }
}
