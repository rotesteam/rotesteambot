/**
 * The ID of a Space may change if the space is upgraded to a newer version (room.upgraded event).
 * Therefore we assign each space a unique ID that can be stored in an external config, e.g. Keycloak.
 * We keep the mapping from external id to the matrix room id up to date whenever a room.upgraded event comes.
 *
 */

import * as fsPromises from 'fs/promises';
import {existsSync} from 'fs';
import {randomUUID} from 'crypto';

import {LogService} from 'matrix-bot-sdk';

export class ExternalSpaceId {
  constructor(public id: string) {}
}

export default class Spaces {
  private currentData: Promise<SpaceMapping>;

  constructor(private persistPath: string) {
    this.currentData = this.loadData();
  }

  private async loadData(): Promise<SpaceMapping> {
    if (existsSync(this.persistPath)) {
      LogService.debug('Spaces', 'restoring mapping');
      return SpaceMapping.fromJSON(
        await fsPromises.readFile(this.persistPath, {encoding: 'utf-8'})
      );
    } else {
      return new SpaceMapping(new Map(), new Map());
    }
  }

  private async persistData() {
    LogService.debug('Spaces', 'persisting mapping');
    await fsPromises.writeFile(
      this.persistPath,
      (await this.currentData).toJSON(),
      {encoding: 'utf-8'}
    );
  }

  public async getExternalID(roomID: string): Promise<ExternalSpaceId> {
    const storedKey = (await this.currentData).internalToExternal.get(roomID);
    if (storedKey == undefined) {
      await this.storeMapping(randomUUID(), roomID);
      return this.getExternalID(roomID);
    }
    return new ExternalSpaceId(storedKey);
  }

  public async alreadyKnowsRoom(roomID: string): Promise<boolean> {
    return (await this.currentData).internalToExternal.has(roomID);
  }

  public async getRoomID(externalID: ExternalSpaceId): Promise<string> {
    const internal = (await this.currentData).externalToInternal.get(
      externalID.id
    );
    if (!internal) {
      throw new Error(`Could not find externalSpace id ${externalID}`);
    }
    return internal;
  }

  public async updateID(oldRoomID: string, newRoomID: string) {
    const externalID = await this.getExternalID(oldRoomID);
    await this.storeMapping(externalID.id, newRoomID);
  }

  private async storeMapping(externalID: string, internalID: string) {
    LogService.debug(
      'Spaces',
      `setting mapping: Matrix-Room ${internalID} has external ID: ${externalID}`
    );
    const data = await this.currentData;
    data.externalToInternal.set(externalID, internalID);
    data.internalToExternal.set(internalID, externalID);
    await this.persistData();
  }
}
class SpaceMapping {
  constructor(
    public externalToInternal: Map<string, string>,
    public internalToExternal: Map<string, string>
  ) {}

  public static fromJSON(json: string): SpaceMapping {
    const externalToInternal = new Map<string, string>(
      Object.entries(JSON.parse(json))
    );
    // Reconstruct other side
    const internalToExternal = new Map<string, string>();
    for (const [external, roomID] of externalToInternal) {
      internalToExternal.set(roomID, external);
    }
    return new SpaceMapping(externalToInternal, internalToExternal);
  }

  public toJSON(): string {
    // We store only one side of the mapping and reconstruct the other one on loading
    return JSON.stringify(Object.fromEntries(this.externalToInternal));
  }
}
